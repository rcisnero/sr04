from datetime import date, datetime
from configparser import ConfigParser
import time

# etats clim et chauffage
etat_clim = 0
etat_chauffage = 0

# constantes liées à la détection de mouvement
lastMovementDate = 0
sbWasRecentlyHere = False

# constantes liées au temps
liste_jour=[0,1,2,3]
seasons = [('winter', ('1,1', '3, 20')),
           ('spring', ('3, 21', '6, 20')),
           ('summer', ('6, 21', '9, 22')),
           ('autumn', ('9, 23', '12, 20')),
           ('winter', ('12, 21', '12, 31'))]

def read_config_file():
    global t_min
    global t_max
    global h_max
    global h_min
    global room
    global saison
    global heure_coucher, heure_lever, heure_depart, heure_retour

    saison = get_season()
    configur = ConfigParser()
    configur.read('config.ini')
    #print("Sections : ", configur.sections())

    # get room
    room = configur.get('home values', 'room')
    print("Home room : ", room)
    heure_coucher = int(configur.get('time', 'heure_coucher'))
    heure_lever   = int(configur.get('time', 'heure_lever'))
    heure_depart  = int(configur.get('time', 'heure_depart'))
    heure_retour  = int(configur.get('time', 'heure_retour'))

    # get isChild
    age = int(configur.get('home values', 'age'))
    if age < 15:
        isChild = True
    else :
        isChild = False

    h_min = float(configur.get('humidity', 'h_min'))
    h_max = float(configur.get('humidity', 'h_max'))

    # get min and max temperature values
    if room == "bedroom":
        if isChild is True:
            if saison is 'winter':
                t_min = float(configur.get('temperature', 't_min_hiver_chambre_enfant'))
                t_max = float(configur.get('temperature', 't_max_hiver_chambre_enfant'))
            elif saison is 'summer':
                t_min = float(configur.get('temperature', 't_min_ete_chambre_enfant'))
                t_max = float(configur.get('temperature', 't_max_ete_chambre_enfant'))
            else:
                # saison = printemps ou automne
                t_min = float(configur.get('temperature', 't_min_hiver_chambre_enfant'))
                t_max = float(configur.get('temperature', 't_max_ete_chambre_enfant'))
        else:
            if saison is 'winter':
                t_min = float(configur.get('temperature', 't_min_hiver_chambre_adulte'))
                t_max = float(configur.get('temperature', 't_max_hiver_chambre_adulte'))
            elif saison is 'summer':
                t_min = float(configur.get('temperature', 't_min_ete_chambre_adulte'))
                t_max = float(configur.get('temperature', 't_max_ete_chambre_adulte'))
            else:
                # saison = printemps ou automne
                t_min = float(configur.get('temperature', 't_min_hiver_chambre_adulte'))
                t_max = float(configur.get('temperature', 't_max_ete_chambre_adulte'))

    else:
        # piece qui n'est pas une chambre
        if saison is 'winter':
            t_min = float(configur.get('temperature', 't_min_hiver_autre_piece'))
            t_max = float(configur.get('temperature', 't_max_hiver_autre_piece'))
        elif saison is 'summer':
            t_min = float(configur.get('temperature', 't_min_ete_autre_piece'))
            t_max = float(configur.get('temperature', 't_max_ete_autre_piece'))
        else:
            # saison = printemps ou automne
            t_min = float(configur.get('temperature', 't_min_hiver_autre_piece'))
            t_max = float(configur.get('temperature', 't_max_ete_autre_piece'))


def get_season():
    now = datetime.today().strftime("%m,%d")
    return next(season for season, (start, end) in seasons
                if start <= now <= end)

def stop_clim() :
    if etat_clim == 1 :
        etat_clim == 0
        print("On éteint le climatiseur.")
    else :
        pass
        #print("Le climatiseur est déjà éteint.")

def start_clim() :
    if etat_clim == 0 :
        etat_clim == 1
        print("On allume le climatiseur.")
    else :
        pass
        #print("Le climatiseur est déjà allumé.")

def stop_chauffage() :
    if etat_chauffage == 1 :
        etat_chauffage == 0
        print("On éteint le chauffage.")
    else :
        pass
        #print("Le chauffage est déjà éteint.")

def start_chauffage() :
    if etat_chauffage == 0 :
        etat_chauffage == 1
        print("On allume le chauffage.")
    else :
        pass
        #print("Le chauffage est déjà allumé.")

def set_temperature(currentTemp, newTemp) :

    if currentTemp == newTemp :
        print("Temperature réglée à ", newTemp)

    elif currentTemp < newTemp :
        # il faut augmenter la température
        stop_clim()
        if etat_chauffage == 0 and (saison == "winter" or saison == "autumn"):
            start_chauffage()
        print("Temperature réglée à ", newTemp)

    else :
        # il faut baisser la température
        stop_chauffage()
        if etat_clim == 0 and saison == "summer":
            start_clim()
        print("Temperature réglée à ", newTemp)



def aeration() :
    print("On aére la maison pendant 10 minutes.")

def humidificateur() :
    print("On humidifie la maison pendant 10 minutes.")

def regulate(humidite, temperature, movementOccured) :
    print("\nDonnées courantes : T =",temperature,"H =",humidite)
    global t_min
    global t_max
    global h_max
    global h_min
    global room

    global lastMovementDate
    global sbWasRecentlyHere
    global saison

    saison = get_season()
    jour = datetime.today().weekday()
    heure = int(datetime.now().strftime("%H"))

    #print("Heure =",heure,"jour =",jour,"saison =",saison)

    # manage movement
    if movementOccured is True:
        lastMovementDate = time.time() # time in sec since epoch (float)
        sbWasRecentlyHere = True

    if lastMovementDate < time.time() - 3 * 60 :
        print("Personne dans la piece")
        sbWasRecentlyHere = False

    # action selon le moment de la journée
    if (jour in liste_jour and heure >= heure_retour - 1 and heure <= heure_retour) or (heure <= heure_lever and heure >= heure_lever - 1) :
        # on adapte la temperature 1h avant l'occupation de la maison
        print("Heure -1 avant occupation de la maison")
        if temperature < t_min :
            set_temperature(temperature, t_min)
        if temperature > t_max :
            set_temperature(temperature, t_max)

    elif (heure >= heure_coucher) or (heure <= heure_lever) :
        # on éteint le chauffage la nuit
        print("Heure de sommeil")
        #stop_clim()
        #stop_chauffage()
        if room != "bedroom" and saison == 'winter':
            # si la piece n'est pas une chambre, on la maintient à faible température durant la nuit en hiver, on laisse refroidir un peu
            set_temperature(temperature, t_min - 2)

    elif jour in liste_jour and heure >= heure_depart and heure <= heure_retour :
        print("Heures de travail")
        #stop_clim()
        #stop_chauffage()
        if saison == 'winter':
            set_temperature(temperature, t_min - 2)
        if humidite > h_max :
            aeration()
        elif humidite < h_min :
            humidificateur()

    else :
        # pendant que la maison est occupée, on la maintient à une température et à une humidité acceptables
        print("Heures d'occupation de la maison")

        if sbWasRecentlyHere is False :
             if saison == 'winter':
                set_temperature(temperature, t_min)
             elif saison == 'summer':
                set_temperature(temperature, t_max)
        else :
            print("Quelqu'un est dans la pièce, dernier mouvement ", time.ctime(lastMovementDate))

    # en général, on laisse la personne régler la température comme elle le souhaite manuellement, à partir du moment où elle n'entraîne pas une surconsommation
    if temperature > t_max and (saison == 'winter' or saison == 'autumn'):
        set_temperature(temperature, t_max)
    if temperature < t_min and (saison == 'summer' or saison == 'spring'):
        set_temperature(temperature, t_min)


def optimize(temperature, interval):
    global t_min
    global t_max
    if temperature == None: 
        print("Error sensor : interval unchanged")
        return interval
    if temperature > t_max or temperature < t_min :
        print("interval=", interval/2)
        return interval/2
    else :
        diff = min(temperature-t_min,t_max-temperature)
        print("interval=", interval*(diff+1))
        return interval*(diff+1)

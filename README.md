# Projet SR04 - L'IoT pour l'environnement

## Auteurs
CISNEROS ARAUJO Ricardo
COYEN Laurène
DAVIET Paul
ZHU Jing

## Principe
Ce projet a pour but le réglage automatique et intelligent de la température selon différents critères qui ont été présentés dans le rapport et la soutenance (saison, type de pièce, personne étant dans la pièce s'il s'agit d'une chambre, horaires d'occupation de la maison et de sommeil des habitants). De plus, ce projet permet le monitoring de la température et de l'humidité de la pièce grâce à l'utilisation d'une base de données InfluxDb et d'un dashboard réalisé avec Grafana.

## Lancement du programme
Pour lancer le programme, veuillez utiliser la commande `python3 both_sensors.py` en ayant réalisé le montage sur la Raspberry Pi avec les différents capteurs tel que spécifié dans le rapport.

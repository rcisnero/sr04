import RPi.GPIO as GPIO
import Adafruit_DHT
import time
from influxdb import InfluxDBClient
from configparser import ConfigParser
from regulation import regulate, read_config_file, optimize

# Configure InfluxDB connection variables
host = "localhost" # IP address of the Raspberry
port = 8086 # default port
user = "rpi" # the user/password created for the pi, with write access
password = "rpi"
dbname = "sensor_data" # the database we created earlier
interval = 180 # Sample period in seconds

# InfluxDB config
client = InfluxDBClient(host, port, user, password, dbname)
measurement = "DHT11"

# motion sensor
MOTION_SENSOR_PIN = 17
# temperature and humidity sensor
DHT_SENSOR = Adafruit_DHT.DHT11
DHT_PIN = 24


def start():

    # BCM numbering system : it refers to the channel numbers on the Broadcom SOC
    GPIO.setmode(GPIO.BCM)
    # configure channel as an input
    GPIO.setup(MOTION_SENSOR_PIN, GPIO.IN)
    # add rising edge detection on a channel on motion sensor
    GPIO.add_event_detect(MOTION_SENSOR_PIN, GPIO.RISING)

    movementOccured = False

    while True:
        # get motion
        if GPIO.event_detected(MOTION_SENSOR_PIN):
            print('There was a movement!')
            movementOccured = True

        # get humidity and tempertature
        humidity, temperature = Adafruit_DHT.read(DHT_SENSOR, DHT_PIN)
        if humidity is not None and temperature is not None:
            #print("temperature ={0:0.1f}C humidity={1:0.1f}%".format(temperature, humidity))

            # Create the JSON data structure
            data = [
            {
                "measurement": measurement,
                "fields": {
                    "temperature" : temperature,
                    "humidity": humidity
                }
            }
            ]

            # Send the JSON data to InfluxDB
            client.write_points(data)

            # Regulate humidity and temperature
            regulate(humidity, temperature, movementOccured);
            movementOccured = False

        else:
            print("Erreur de lecture du capteur.")

        # wait
        time.sleep(optimize(temperature,interval))


if __name__ == "__main__":
    try:
        read_config_file()
        start()
    except KeyboardInterrupt:
        print("Finish...")
        GPIO.cleanup()
        exit()
